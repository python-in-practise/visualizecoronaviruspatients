import pandas as pd;
from os import listdir;
from os.path import isfile, join;
from datetime import datetime;
import plotly.express as px;
import sys;

top=200;
def getAllCountriesCoronaCasesCountByType(casesType):
	totalCasesDict={};
	dates=[];
	casesCount=[];
	countries=[];
	#Get all CSV files list in the current directory
	allCSVFilesList = [f for f in listdir("./") if isfile(join("./", f)) if(f.endswith('.csv'))]	

	#read all csv files
	for fileName in allCSVFilesList:
		try:
			csvData=pd.read_csv(fileName,index_col=0,header=0);
			date=csvData.loc['Date'][0];
			for cnt in range(top):
				dates.append(date);		
			for caseType in csvData[casesType][9:(9+top)]:
				casesCount.append(caseType);
			for country in csvData.index.values[9:(9+top)]:
				countries.append(country);
		except:
			casesType=input("Seems like you entered invalid type.Please check the above list and re-enter or type 'exit' to close : ");
			if casesType=="exit":
				sys.exit();
			else:
				return getAllCountriesCoronaCasesCountByType(casesType);
	totalCasesDict['Date']=dates;
	totalCasesDict['Count']=casesCount;
	totalCasesDict['Countries']=countries;

	#Create DataFrame from Dict
	df = pd.DataFrame(totalCasesDict);
	df=df.sort_values('Date');
	return df;


#Read user entered country
def getCasesTypeFromUser():
	casesType = input("Select and Type any one country from the following List :\nTotalCases\nNewCases\nTotalDeaths\nNewDeaths\nTotalRecovered\nActiveCases\nSerious,Critical\n>>");
	return casesType;

#Visualize function
def visualizeData(df):	
	fig = px.line(df, x="Date", y="Count",color="Countries",title="Top "+str(top)+" countries affected by Corona Virus : "+casesType+" count");
	fig.show();

def cleanData(df):
	df['Count'].fillna(0,inplace = True);
	return df;



if __name__ == "__main__":  
	try:
		casesType=getCasesTypeFromUser();
		#Get corona cases count country wise
		df=getAllCountriesCoronaCasesCountByType(casesType);
		#Clean data before visualization
		df=cleanData(df);
		#Visualize the corona virus data
		visualizeData(df);
	except Exception as e:
		print("Something went wrong. Data not visualized. ERROR : ",e);


