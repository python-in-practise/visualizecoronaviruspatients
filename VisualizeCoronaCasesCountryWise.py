import pandas as pd;
from os import listdir;
from os.path import isfile, join;
from datetime import datetime;
import plotly.express as px;
import sys;

def getCoronaCasesCountByCountry(country):
	totalCasesDict={};
	dates=[];
	casesCount=[];
	casesType=[];
	#Get all CSV files list in the current directory
	allCSVFilesList = [f for f in listdir("./") if isfile(join("./", f)) if(f.endswith('.csv'))]	

	#read all csv files
	for fileName in allCSVFilesList:
		try:
			csvData=pd.read_csv(fileName,index_col=0,header=0);
			dates.append(csvData.loc['Date'][0]);
			casesCount.append(csvData.loc[country]['TotalCases']);
			casesType.append('TotalCases');
			
			dates.append(csvData.loc['Date'][0]);
			casesCount.append(csvData.loc[country]['NewCases']);
			casesType.append('NewCases');

			dates.append(csvData.loc['Date'][0]);
			casesCount.append(csvData.loc[country]['TotalDeaths']);
			casesType.append('TotalDeaths');

			dates.append(csvData.loc['Date'][0]);
			casesCount.append(csvData.loc[country]['NewDeaths']);
			casesType.append('NewDeaths');

			dates.append(csvData.loc['Date'][0]);
			casesCount.append(csvData.loc[country]['TotalRecovered']);
			casesType.append('TotalRecovered');

			dates.append(csvData.loc['Date'][0]);
			casesCount.append(csvData.loc[country]['ActiveCases']);
			casesType.append('ActiveCases');

			dates.append(csvData.loc['Date'][0]);
			casesCount.append(csvData.loc[country]['Serious,Critical']);
			casesType.append('Serious,Critical');		
		except Exception:
			country=input("Seems like you entered invalid country name.Please check the above list and re-enter or type 'exit' to close : ");
			if country=="exit":
				sys.exit();
			else:
				return getCoronaCasesCountByCountry(country);

	totalCasesDict['Date']=dates;
	totalCasesDict['Count']=casesCount;
	totalCasesDict['Cases_Type']=casesType;

	#Create DataFrame from Dict
	df = pd.DataFrame(totalCasesDict)
	df=df.sort_values('Date')
	return df;

#Read user entered country
def getCountryFromUser():
	country = input("Select and Type any one country from the following List :\n China\nUSA\nSpain\nItaly\nUK\nFrance\nGermany\nTurkey\nRussia\nIran\nBrazil\nCanada\nBelgium\nNetherlands\nPeru\nIndia\nSwitzerland\nPortugal\nEcuador\nSaudi Arabia\nSweden\nIreland\nMexico\nPakistan\nSingapore\nChile\nIsrael\nAustria\nJapan\nBelarus\nQatar\nPoland\nUAE\nRomania\nS. Korea\nUkraine\nIndonesia\nDenmark\nSerbia\nPhilippines\nNorway\nCzechia\nBangladesh\nDominican Republic\nAustralia\nColombia\nPanama\nMalaysia\nSouth Africa\nEgypt\nFinland\nArgentina\nMorocco\nKuwait\nAlgeria\nMoldova\nLuxembourg\nKazakhstan\nBahrain\nThailand\nHungary\nGreece\nOman\nAfghanistan\nIraq\nCroatia\nGhana\nArmenia\nUzbekistan\nNigeria\nCameroon\nzerbaijan\nIceland\nBosnia and Herzegovina\nEstonia\nBulgaria\nCuba\nGuinea\nNew Zealand\nNorth Macedonia\nSlovenia\nSlovakia\nLithuania\nIvory Coast\nBolivia\nDjibouti\nHong Kong\nTunisia\nSenegal\nLatvia\nCyprus\nAlbania\nHonduras\nKyrgyzstan\nAndorra\nLebanon\nNiger\nCosta Rica\nDiamond Princess\nSri Lanka\nBurkina Faso\nUruguay\nSomalia\nGuatemala\nDRC\nSan Marino\nGeorgia\nMayotte\nChannel Islands\nMali\nTanzania\nMaldives\nMalta\nJordan\nSudan\nTaiwan\nRéunion\nKenya\nJamaica\nEl Salvador\nPalestine\nVenezuela\nMauritius\nMontenegro\nIsle of Man\nEquatorial\nGuinea\nGabon\nVietnam\nParaguay\nRwanda\nCongo\nGuinea-Bissau\nFaeroe Islands\nMartinique\nGuadeloupe\nMyanmar\nGibraltar\nLiberia\nBrunei\nEthiopia\nMadagascar\nFrench Guiana\nSierra Leone\nCambodia\nCabo Verde\nTogo\nTrinidad and Tobago\nBermuda\nZambia\nAruba\nEswatini\nMonaco\nUganda\nGuyana\nLiechtenstein\nBahamas\nHaiti\nBarbados\nMozambique\nSint Maarten\nChad\nCayman Islands\nBenin\nLibya\nFrench Polynesia\nNepal\nCAR\nMacao\nSyria\nZimbabwe\nEritrea\nSaint Martin\nMongolia\nMalawi\nSouth Sudan\nAngola\nAntigua and Barbuda\nTimor-Leste\nBotswana\nGrenada\nLaosBelize\nFiji\nNew Caledonia\nSaint Lucia\nCuraçao\nDominica\nNamibia\nSt. Vincent Grenadines\nSaint Kitts and Nevis\nTajikistan\nNicaragua\nSao Tome and Principe\nFalkland Islands\nTurks and Caicos\nBurundi\nGambia\nMontserrat\nGreenland\nVatican City\nSeychelles\nSuriname\nMS Zaandam\nMauritania\nPapua New Guinea\nBhutan\nYemen\nBritish Virgin Islands\nSt. Barth\nWestern Sahara\nCaribbean Netherlands\nAnguilla\nComoros\nSaint Pierre Miquelon\n>>");
	return country;

#Visualize function
def visualizeData(df):	
	fig = px.line(df, x="Date", y="Count",color="Cases_Type",title="Corona Virus Cases in "+country);
	fig.show();

def cleanData(df):
	df['Count'].fillna(0,inplace = True);
	return df;




if __name__ == "__main__":  
	try:
		#Get country name from command line argument 
		#country=sys.argv[1];
		country=getCountryFromUser();
		#Get corona cases count country wise
		df=getCoronaCasesCountByCountry(country);
		#Clean data before visualization
		df=cleanData(df);
		#Visualize the corona virus data
		visualizeData(df);
	except Exception as e:
		print("Something went wrong. Data not visualized. ERROR : ",e);


